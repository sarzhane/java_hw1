package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println("ДЗ условные операторы");
        Conditions conditions = new Conditions();
        conditions.task1(-1, -25);
        conditions.task2(8, 5);
        conditions.task3(3,4,5);
        conditions.task4(7,8,9);
        conditions.task5(-25);

        System.out.println("ДЗ циклы");
        Cycle cycle = new Cycle();
        cycle.task1();
        cycle.task2(7);
        cycle.task3(24);
        cycle.task4(6);
        cycle.task5(37);
        cycle.task6(25);

        System.out.println("ДЗ массивы");
        Array array = new Array();
        int[]arr = new int[]{1,2,3,45,6};
        array.task1(arr);
        array.task2(arr);
        array.task3(arr);
        array.task4(arr);
        array.task5(arr);
        array.task6(arr);

        System.out.println("ДЗ функции");
        Function function = new Function();
        function.task1(3);
    }
}
