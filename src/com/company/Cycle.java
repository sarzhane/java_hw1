package com.company;

public class Cycle {
    /* Найти сумму четных чисел и их количество в диапазоне от 1 до 99*/
    public void task1() {
        int sum = 0;
        int count = 0;

        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                sum += i;
                count++;
            }
        }
        System.out.println("Сумма четных чисел = " + sum);
        System.out.println("Количество четных чисел в диапазоне от 1 до 99 = " + count);

    }

    /* Проверить простое ли число? (число называется простым, если оно делится только
       само на себя и на 1)*/
    public void task2(int number) {
        int i = 2;
        while (i < number) {
            if (number % i != 0)
                i++;
        }
        if (i == number)
            System.out.println(number + " is a natural number.");
        else {
            System.out.println(number + " is not a natural number.");
        }
    }

    /* Найти корень натурального числа с точностью до целого (рассмотреть вариант
     последовательного подбора и метод бинарного поиска)*/
    public void task3(int a) {
        System.out.println("UPS!");

    }

    /* Вычислить факториал числа n. n! = 1*2*…*n-1*n;!*/
    public void task4(int number) {
        int factorial = 1;
        if (number > 1) {
            for (int i = 1; i <= number; i++) {
                factorial *= i;
            }
        }
        System.out.println("Factorial = " + factorial);
    }

    /* Посчитать сумму цифр заданного числа*/
    public void task5(int number) {
        int result = 0;
        while (number != 0) {
            result += number % 10;
            number /= 10;
        }
        System.out.println("Result: " + result);
    }

    /* Вывести число, которое является зеркальным отображением последовательности
цифр заданного числа, например, задано число 123, вывести 321.*/
    public void task6(int a) {
        int result = 0;
        while (a > 0) {
            result = (result + (a % 10)) * 10;
            a = a / 10;
        }
        result /= 10;
        System.out.println("Result: " + result);
    }
}
