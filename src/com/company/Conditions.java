package com.company;

public class Conditions {
    /* Определить какой четверти принадлежит точка с координатами (х,у)*/
    public void task1(int x, int y) {
        if (x > 0 && y > 0) {
            System.out.println("Это 1-й квадрант");
        } else if (x < 0 && y > 0) {
            System.out.println("Это 2-й квадрант");
        } else if (x < 0 && y < 0) {
            System.out.println("Это 3-й квадрант");
        } else if (x > 0 && y < 0) {
            System.out.println("Это 4-й квадрант");
        } else if (x == 0 && y == 0) {
            System.out.println("Это победа!");
        }
    }

    /* Найти Если а – четное посчитать а*б, иначе а+б*/
    public void task2(int a, int b) {
        int result;
        if (a % 2 == 0) {
            result = a * b;
        } else {
            result = a + b;
        }
        System.out.println("Результат: " + result);
    }

    /* Суммы только положительных из трех чисел*/
    public void task3(int a, int b, int c) {
        int result = 0;
        if (a > 0) {
            result += a;
        }  if (b > 0) {
            result += b;
        }  if (c > 0) {
            result += c;
        }
        System.out.println("Результат: " + result);

    }

    /* Посчитать выражение макс(а*б*с, а+б+с)+3*/
    public void task4(int a, int b, int c) {
        int result = 0;
        int productOfNumbers = a * b * c;
        int sum = a + b + c;
        if (productOfNumbers > sum) {
            result = productOfNumbers + 3;
        } else {
            result = sum + 3;
        }
        System.out.println("Результат: " + result);
    }

    /* Написать программу определения оценки студента по его рейтингу,
         на основе следующих правил*/
    public void task5(int rating) {
        if (rating >= 0 && rating < 20) {
            System.out.println("Your rate: F");
        } else if (rating >= 20 && rating < 40) {
            System.out.println("Your rate: E");
        } else if (rating >= 40 && rating < 60) {
            System.out.println("Your rate: D");
        } else if (rating >= 60 && rating < 75) {
            System.out.println("Your rate: C");
        } else if (rating >= 75 && rating < 90) {
            System.out.println("Your rate: B");
        } else if (rating >= 90 && rating < 101) {
            System.out.println("Your rate: A");
        } else {
            System.out.println("Out of Rating");
        }

    }

}
