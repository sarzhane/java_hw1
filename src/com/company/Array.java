package com.company;

public class Array {
    /* Найти минимальный элемент массива*/
    public void task1(int[] array) {
        int result = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < result) {
                result = array[i];
            }
        }
        System.out.println("Min element = " + result);
    }

    /* Найти максимальный элемент массива*/
    public void task2(int[] array) {
        int result = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > result) {
                result = array[i];
            }
        }
        System.out.println("Max element = " + result);
    }

    /* Найти индекс минимального элемента массива*/
    public void task3(int[] array) {
        int result = array[0];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < result) {
                result = array[i];
                index = i;
            }
        }
        System.out.println("Min index = " + index);
    }

    /* Найти индекс максимального элемента массива*/
    public void task4(int[] array) {
        int result = array[0];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > result) {
                result = array[i];
                index = i;
            }
        }
        System.out.println("Max index = " + index);
    }

    /* Посчитать сумму элементов массива с нечетными индексами*/
    public void task5(int[] array) {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 != 0) {
                result += array[i];
            }
        }
        System.out.println("Result: " + result);
    }

    /* Сделать реверс массива (массив в обратном направлении) Посчитать количество
нечетных элементов массива*/
    public void task6(int[] array) {
        int[] responce = new int[array.length];
        int currentPosition = array.length - 1;
        for (int i = 0; i < array.length; currentPosition--, i++) {
            responce[currentPosition] = array[i];
        }
    }
}
